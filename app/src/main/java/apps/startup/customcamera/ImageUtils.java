package apps.startup.customcamera;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.net.URL;
import java.net.URLConnection;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.List;

public class ImageUtils {

    public static ImageUri compressImageFile(ImageUri imageUri,Context context) throws IOException {
        File f=getImagefile(context);
        FileOutputStream fout=new FileOutputStream(f);
        Bitmap bitmap=MediaStore.Images.Media.getBitmap(context.getContentResolver(), imageUri.getUri());
        bitmap=Bitmap.createScaledBitmap(bitmap,(bitmap.getWidth()*50)/100,(bitmap.getHeight()*50)/100,true);
        bitmap.compress(Bitmap.CompressFormat.JPEG, 10, fout);
        ImageUri compressedImageUri=new ImageUri();
        compressedImageUri.setUri(FileProvider.getUriForFile(context, "abcd", f));
        return compressedImageUri;
    }

    public static ArrayList<ImageUri> compressImageList(ArrayList<ImageUri> imageUriArrayList,Context context) throws IOException {
        ArrayList<ImageUri> newImageUriList=new ArrayList<>();
        for(ImageUri imageUri :imageUriArrayList)
        {
            newImageUriList.add(compressImageFile(imageUri,context));
        }
        return newImageUriList;
    }

    public static File getImagefile(Context context) throws IOException {
        File storageDir;
        String imageFileName = "JPEG_" + "Loconav" + "_";
        storageDir = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,
                ".jpg",
                storageDir
        );
        return image;
    }

    public static Bitmap getThumbnailImage(Uri image,Context context) throws IOException
    {
        final int THUMBSIZE = 256;
        Log.e("the image is ","the big image path is "+image.getPath());
        return  ThumbnailUtils.extractThumbnail(BitmapFactory.decodeStream(context.getContentResolver().openInputStream(image)),
                THUMBSIZE, THUMBSIZE);
    }


}
