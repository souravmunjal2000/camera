package apps.startup.customcamera;

import android.app.Application;
import android.databinding.DataBindingUtil;


/**
 * Created by prateek on 16/02/18.
 */
public class LookUpApplication extends Application {

    private static LookUpApplication instance = null;

    public static LookUpApplication getInstance(){
        return instance;
    }
    @Override
    public void onCreate() {
        super.onCreate();
//        if (LeakCanary.isInAnalyzerProcess(this)) {
//            return;
//        }
//        LeakCanary.install(this);
        instance = this;
//        Fabric.with(this, new Crashlytics());
        DataBindingUtil.setDefaultComponent(new MyDataBindingComponent());
    }
}
