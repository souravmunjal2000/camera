package apps.startup.customcamera;

import android.Manifest;
import android.databinding.DataBindingUtil;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;

import apps.startup.customcamera.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity {
    ActivityMainBinding binding;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.CAMERA},3);
        binding= DataBindingUtil.setContentView(this,R.layout.activity_main);
        binding.click.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ArrayList<ImageUri> ss=binding.DeviceFitting.getimagesList();
                Toast.makeText(MainActivity.this, ""+ss.size(), Toast.LENGTH_SHORT).show();

            }
        });
    }
}
