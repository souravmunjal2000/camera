package apps.startup.customcamera;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;


import apps.startup.customcamera.databinding.ActivityCameraOpenBinding;

public class CameraOpenActivity extends AppCompatActivity {
    ActivityCameraOpenBinding cameraOpenBinding;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        cameraOpenBinding= DataBindingUtil.setContentView(this,R.layout.activity_camera_open);
        FragmentController fragmentController=new FragmentController();
        Bundle bundle=new Bundle();
        bundle.putInt("limit",getIntent().getExtras().getInt("limit"));
        bundle.putString("Stringid",getIntent().getExtras().getString("Stringid"));
        CameraPickerFragment cameraPickerFragment=new CameraPickerFragment();
        cameraPickerFragment.setArguments(bundle);
        fragmentController.loadFragment(cameraPickerFragment,getSupportFragmentManager(),R.id.camera,false);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        cameraOpenBinding.unbind();
    }
}
